import * as nano from 'nanocurrency';
import { EncryptedStorage } from './encrypted-storage';
import { BlockLedger } from './block-ledger';
import BigNumber from "big.js";

/*
encrypted interface WalletSecrets {
    seed: string;
    privateKey: string;
}
*/

async function brandSpankinNewWallet() {
    const seed = await nano.generateSeed();
    const privateKey = nano.deriveSecretKey(seed, 0);
    const publicKey = nano.derivePublicKey(privateKey);
    const address = nano.deriveAddress(publicKey);

    return {
        secrets: {
            seed: seed,
            privateKey: privateKey,
        },
        info: {
            publicKey: publicKey,
            address: address,
        }
    }
}

export function test() {

}

const storage = EncryptedStorage(promptForNewPin);
const walletStorageKey = 'nano_wallet';
let isNewUser = !storage.hasItem(walletStorageKey);
let ledger;

async function promptForNewPin() {
    return '1234';
}

async function getWalletSecrets() {
    return JSON.parse(await storage.getItem(walletStorageKey));
}

function getWalletInfo() {
    return JSON.parse(localStorage.getItem(walletStorageKey));
}

async function getThisWallet() {
    const walletInfo = getWalletInfo();
    const frontierBlock = await ledger.frontier();
    return {
        address: walletInfo.address,
        publicKey: walletInfo.publicKey,
        frontierHash: frontierBlock ? frontierBlock.hash : null,
        balance: frontierBlock ? frontierBlock.balance : null,
    };
}

export const Wallet = {
    get address() {
        return getWalletInfo().address;
    },
    async create() {
        if (!isNewUser) {
            const { address } = JSON.parse(localStorage.getItem(walletStorageKey));
            ledger = BlockLedger(address);
            return false;
        }

        const wallet = await brandSpankinNewWallet();
        ledger = BlockLedger(wallet.info.address);

        await storage.setItem(walletStorageKey, JSON.stringify(wallet.secrets));
        localStorage.setItem(walletStorageKey, JSON.stringify(wallet.info));
        return true;
    },
    async receive(sendBlockHash, amtReceiving, onProgressCb) {
        const receivingWallet = await getThisWallet();

        const openEquiv = !receivingWallet || !receivingWallet.frontierHash;
        const newBalance = openEquiv ? new BigNumber(amtReceiving) : new BigNumber(receivingWallet.balance).plus(amtReceiving);
        const previousBlock = receivingWallet.frontierHash || "0000000000000000000000000000000000000000000000000000000000000000";

        const { block, hash } = nano.createBlock((await getWalletSecrets()).privateKey, {
            balance: newBalance.toString(10),
            representative: 'nano_3dmtrrws3pocycmbqwawk6xs7446qxa36fcncush4s1pejk16ksbmakis78m',
            link: sendBlockHash,
            previous: previousBlock,
            work: null,
        });
        await ledger.add(block, hash);
    },
    // interface SendOptions {
    //     destination: string; // Address to send to
    //     amount: string; // How much ya sending
    // }
    async send(sendOptions) {
        const sendingWallet = await getThisWallet();
        if (!sendingWallet.balance || new BigNumber(sendingWallet.balance).lt(sendOptions.amount)) {
            throw new Error(`Insufficeint funds. Balance: ${sendingWallet.balance}`);
        }

        const newBalance = new BigNumber(sendingWallet.balance).minus(sendOptions.amount);

        const { block, hash } = nano.createBlock((await getWalletSecrets()).privateKey, {
            balance: newBalance.toString(10),
            representative: 'nano_3dmtrrws3pocycmbqwawk6xs7446qxa36fcncush4s1pejk16ksbmakis78m',
            link: sendOptions.destination,
            work: null,
            previous: sendingWallet.frontierHash,
        });
        await ledger.add(block, hash);
    },
    getInfo() {
        return getThisWallet();
    }
};
