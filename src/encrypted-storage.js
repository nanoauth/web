const SALT = new TextEncoder().encode('nanoauthStaticSalt');
const IV = new ArrayBuffer(16)

async function getKeys(password) {
    const enc = new TextEncoder();
    const baseKey = await window.crypto.subtle.importKey(
        "raw",
        enc.encode(password),
        { "name": "PBKDF2" },
        false,
        ["deriveKey"]);

    return window.crypto.subtle.deriveKey(
        {
            "name": "PBKDF2",
            "salt": SALT,
            "iterations": 1000,
            "hash": "SHA-1"
        },
        baseKey,
        { "name": "AES-CBC", "length": 128 }, // Key we want
        false,                                // Extrable
        ["encrypt", "decrypt"]                // For new key
    );
}

function toBase64(uint8array) { // return string
    return btoa(String.fromCharCode(...uint8array));
}

function fromBase64(string) { // return Uint8Array
    return new Uint8Array(Array.from(atob(string)).map(c => c.charCodeAt(0)));
}

export function EncryptedStorage(getPassword) {
    return {
        async getItem(key) {
            const td = new TextDecoder();
            const encryptedVal = localStorage.getItem(`$e_${key}`);
            if (!encryptedVal) {
                return null;
            }

            const encryptedValBytes = fromBase64(encryptedVal);
            const keys = await this.loadCryptoKeys();
            const decryptedValBytes = await crypto.subtle.decrypt(
                {
                    name: "AES-CBC",
                    iv: IV,
                },
                keys,
                encryptedValBytes
            );

            return td.decode(decryptedValBytes);
        },
        async setItem(key, strVal) {
            if (!strVal) {
                throw new Error('Must provide a value to encrypt');
            }

            const te = new TextEncoder();
            const td = new TextDecoder();
            const encodedVal = te.encode(strVal);
            const keys = await this.loadCryptoKeys();
            const encryptedVal = await crypto.subtle.encrypt(
                {
                    name: "AES-CBC",
                    iv: IV,
                },
                keys,
                encodedVal
            );

            localStorage.setItem(`$e_${key}`, toBase64(new Uint8Array(encryptedVal)));
        },
        hasItem(key) {
            return !!localStorage.getItem(`$e_${key}`);
        },
        async loadCryptoKeys() {
            const password = await getPassword();
            return getKeys(password);
        }
    }
}