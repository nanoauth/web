import Dexie from 'dexie';
import { Api } from './api';

const walletStorageKey = 'nano_wallet';
function getWalletInfo() {
    return JSON.parse(localStorage.getItem(walletStorageKey));
}

export function BlockLedger(walletAddress) {
    const db = new Dexie(`wallet_${walletAddress}`);

    db.version(1).stores({ blocks: '++id' });

    return {
        async frontier() {
            return db.blocks.reverse().first();
        },
        async add(block, blockHash) {
            let hashToPerformWorkOn = block.previous;
            if (!(await this.frontier())) {
                hashToPerformWorkOn = getWalletInfo().publicKey;
            }

            console.log('adding block', hashToPerformWorkOn, getWalletInfo().publicKey);
            await Promise.all([
                db.blocks.add({ ...block, hash: blockHash }),
                Api.broadcastBlock(block, hashToPerformWorkOn),
            ]);
        },
    }
}