const jsonHeaders = {
    headers: {
        "Content-Type": "application/json"
    }
}

export const Api = {
    // interface FaucetResponse {
    //     block: string, // SendBlock Hash
    //     amount: string,
    // }
    async faucet(address) {
        const res = await fetch(`/api/faucet`, {
            method: "post",
            body: JSON.stringify({ address }),
            ...jsonHeaders
        });
        return res.json();
    },
    async broadcastBlock(block, hash) {
        const res = await fetch('/api/broadcast', {
            method: 'POST',
            body: JSON.stringify({ block, hash }),
            ...jsonHeaders
        });
        return res.text();
    },
    // interface AuthInteractionResponse {
    //     client: unknown,
    //     uid: string,
    //     details: Object<unknown>,
    //     params: unknown,
    //     title: string,
    //     addressToSendFundsTo: string,
    // }
    async beginAuthInteraction(uid) {
        const res = await fetch(`/api/interaction/${uid}`);
        return res.json();
    },
    async verifyAuthInteractionComplete(uid, address) {
        const res = await fetch(`/api/interaction/${uid}/status`, {
            method: 'POST',
            body: JSON.stringify({
                address,
            }),
            redirect: "manual",
            ...jsonHeaders,
        });
        return res.json();
    },
    async finalizeAuthInteraction(uid) {
        const res = await fetch(`/api/interaction/${uid}/confirm`, {
            method: 'POST',
            redirect: "manual"
        });
        return res;
    },
    async signup(address) {
        const res = await fetch(`/api/signup`,
            {
                method: 'POST',
                body: JSON.stringify({
                    address,
                }),
                ...jsonHeaders
            });
        return res.text();
    },
    async hash(block) {
        const res = await fetch(`/api/hashblock`, {
            method: 'POST',
            body: JSON.stringify(block),
            ...jsonHeaders
        });
        return res.json();
    }
}