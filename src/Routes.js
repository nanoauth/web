import ApproveModal from './ApproveModal';
import HomeView from './HomeView';
import AuthView from './AuthView';

export default [
    { path: '/approve', component: ApproveModal },
    { path: '/auth', component: AuthView },
    { path: '/', component: HomeView },
];