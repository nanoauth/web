import Navigo from 'navigo';

const router = new Navigo('/#/', true);
router.notFound(() => { 
    console.log('Route not found');
    router.navigate('/');
});

let activatedRoute = null;
let expectingNavigation = false;
let forwardNavigations = 1;
export function withRoutes(routes, setView) {
    routes.forEach(route => {
        router.on(route.path, (params, query) => {
            activatedRoute = { params, query, path: route.path };
            if (!expectingNavigation) {
                forwardNavigations--;
            }

            expectingNavigation = false;
            setView(route.component);
        });
    });

    router.resolve();
    router.navigate(location.hash.split('#').slice(-1)[0] || "/");
}

export function getActivatedRoute() {
    return activatedRoute;
}

export function navigate(url) {
    expectingNavigation = true;
    forwardNavigations++;
    router.navigate(url);
}

export function back() {
    if (forwardNavigations <= 0) {
        forwardNavigations = 0;
        router.navigate('/');
    } else {
        window.history.back();
    }
}

export function pause() {
    router.pause();
}

export function resume() {
    router.resume();
}
